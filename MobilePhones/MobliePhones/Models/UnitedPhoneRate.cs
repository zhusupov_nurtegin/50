﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobilePhones.Models
{
    public class UnitedPhoneRate
    {
        public Phone myPhones { get; set; }
        public ExchangeRates myEchanges { get; set; }
    }
}
