﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobilePhones.Models
{
    public class MobileContext : DbContext
    {
        public DbSet<Phone> Phones { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<ExchangeRates> ExchangeRatess { get; set; }

        public MobileContext(DbContextOptions<MobileContext> options)

            : base(options)

        {

        }
    }
}
