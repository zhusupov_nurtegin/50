﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MobilePhones.Models
{
    public class ExchangeRates
    {
        public int id { get; set; }
       public string CurrencyCode { get; set; }
       public string CurrencyName { get; set; }
        public double CurrencyRate { get; set; }

        public List<ExchangeRates> exchanges = new List<ExchangeRates>();
        
        public void WriteAndGetJson()
        {
            var str = JsonConvert.SerializeObject(exchanges, new Formatting());
            File.WriteAllText("../../../jsonData/currencies.json", str);
        }
    }
}
