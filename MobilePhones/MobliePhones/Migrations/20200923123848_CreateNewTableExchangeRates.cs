﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MobilePhones.Migrations
{
    public partial class CreateNewTableExchangeRates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
               name:"ExchangeRatess",
               columns: table => new
               {
                   id = table.Column<int>(nullable: false)
                       .Annotation("SqlServer:Identity", "1, 1"),
                   CurrencyCode = table.Column<string>(nullable: false),
                   CurrencyName = table.Column<string>(nullable: true),
                   CurrencyRate = table.Column<double>(nullable: true),

               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_ExchangeRatess", x => x.id);
               });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
               name: "ExchangeRatess");
        }
    }
}
